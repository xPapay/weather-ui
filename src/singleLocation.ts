import { defineCustomElement } from "vue";
import OpenWeatherCard from "./components/OpenWeatherCard.vue";
import styles from './assets/index.css';

export default defineCustomElement({ 
	...OpenWeatherCard, 
	styles: [
		styles.replace(/body|html/g, ':host'),
		':host { display: block; }'
	] 
});
